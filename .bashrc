# Don't do anything if not interactive
case $- in
	*i*) ;;
	*) return;;
esac

# bash requires extra setup in some non-GNU OSes to get basic pieces
# like $PATH.
[ -e "$HOME/.bash-shim" ] && . "$HOME/.bash-shim"

findbin () {
	which "$1" >/dev/null 2>/dev/null
}

# The meat of Debian/RHEL /etc/bashrc and .bashrc, excluding braindead parts
# that confuse login/interactive shells
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=10000
HISTFILESIZE=200000
shopt -s checkwinsize
umask 022
function ls { if [ -t 1 ]; then env ls -F "$@"; else env ls "$@"; fi; }

# Assume 256 colors. The 8/256 aliases below allow switching when this turns
# out to be wrong.
if [ "$TERM" = xterm -o "$TERM" = screen ]; then
  export TERM="${TERM}-256color"
fi
alias 8='export TERM="${TERM%-256color}"'
alias 256='export TERM="${TERM%-256color}-256color"'

# Include machine-local settings that won't get wiped out when this file
# is updated
[ -e "$HOME/.bashrc-local" ] && . "$HOME/.bashrc-local"

alias tree="tree -C"
alias eb="exec bash"
# alias n='PATH="./node_modules/.bin:$PATH"'
sc () { screen -dRS ${1:-main}; }

export PATH="$HOME/local/bin":"$PATH":"$HOME/bin"
export MANPATH="$HOME/local/man":"$HOME/local/share/man":"$MANPATH"

if [ -z "$PROMPT_COLOR" ]; then
	if [ `id -u` = 0 ]; then
		PROMPT_COLOR="[0;35m"
	else
		PROMPT_COLOR="[0;36m"
	fi
fi
export PROMPT_COLOR

title() {
	local pwd="$PWD"
	if [ x"$pwd" = x"$HOME" ]; then
		pwd="~"
	else
		pwd="$(basename "$pwd")"
	fi
	echo "$pwd"
}

xterm_title() {
	if [ "$TERM" = 'xterm' -o "$TERM" = 'xterm-256color' \
	     -o "$TERM" = 'dvtm' -o "$TERM" = 'dvtm-256color' ]; then
		printf '\e]0;%s\007' "$(title)"
	fi
}

brief_pwd() {
  findbin perl && ( echo "$PWD" | perl -pne 's[([^/])[^/]+/][$1/]g; s[/(?![^/]+$)][]g' )
}

# Thanks, pyvenv!
REALPS1='\[$TITLE\]\[\e$PROMPT_COLOR\]$(brief_pwd)\$\[\e[0m\] '
PROMPT_COMMAND='TITLE="$(xterm_title)"; PS1="$REALPS1"; history -a'

findbin keychain && eval `keychain --quiet --eval --nocolor --nogui --agents ssh --ignore-missing id_rsa`

if findbin less; then
	export PAGER="less"
	export MORE="less"
fi

export -n BROWSER
for BROWSER in elinks w3m links links2 firefox chromium google-chrome; do
	findbin "$BROWSER" && export BROWSER && break
done

export -n EDITOR
for EDITOR in vim nvi elvis vi nano emacs; do
	findbin "$EDITOR" && export EDITOR && break
done

if [ -z "$GOPATH" ]; then
	export GOPATH="$HOME/go"
fi
alias cdgo="cd $GOPATH/src/bitbucket.org/benselb"

if [ -n "$GOPATH" ]; then
  export PATH="$PATH:$GOPATH/bin"
fi

if [ -d "$HOME/.npm-packages" ]; then
  export PATH="$PATH:$HOME/.npm-packages/bin"
  export MANPATH="$MANPATH:$HOME/.npm-packages/share/man"
  export NODE_PATH="$HOME/.npm-packages:$NODE_PATH"
fi

alias vundle='vim +PluginInstall +qall'

# Disable bizarre coreutils ls pseudo-shell quoting
export QUOTING_STYLE=literal
