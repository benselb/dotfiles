#!/bin/sh
git submodule init
git submodule update

link () {
	# Produce the behavior of -T of GNU ln, -h of BSD
	DST="$2/$(basename $1)"
	printf "%-16.16s -> %s\n" "$(basename $1)" "$2"
	rm -f "$DST"
	mkdir -p "$2"
	ln -fs "$PWD/$1" "$DST"
}

put () {
	DST="$2/$(basename $1)"
	if [ ! -e "$DST" ]; then
		printf "%-16.16s -> %s\n" "$(basename $1)" "$2"
		mkdir -p "$2"
		cp "$PWD/$1" "$DST"
	else
		printf "%-16.16s (skipped)\n" "$(basename $1)"
	fi
}

SHIM="$PWD/shims/$(uname -s)"
if [ -e "$SHIM" ]; then
	echo "bash shim required."
	rm -f "$HOME/.bash-shim"
	ln -fs "$SHIM" "$HOME/.bash-shim"
fi

link .bash_profile "$HOME"
link .bashrc "$HOME"
link compton.conf "$HOME/.config"
link .gitconfig "$HOME"
link gradle.properties "$HOME/.gradle"
link .i3 "$HOME"
put .i3status.conf "$HOME"
link .inputrc "$HOME"
link .moc "$HOME"
link .nethackrc "$HOME"
link .npmrc "$HOME"
link .screenrc "$HOME"
link .tmux.conf "$HOME"
put .tmux.conf-local "$HOME"

link .vim "$HOME"
mkdir -p .vim/autoload
mkdir -p .vim/bundle
link ext/Vundle.vim "$HOME/.vim/bundle"
mkdir -p .vim/colors
link ext/vim-monokai/colors/monokai.vim "$HOME/.vim/colors"
link .vimrc "$HOME"
link .vim-formatting "$HOME"

link config.fish "$HOME/.config/fish"
put terminalrc "$HOME/.config/Terminal"
