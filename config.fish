set -e fish_greeting

function fish_right_prompt
	echo (echo -e "\e$PROMPT_COLOR")(date "+%a %H:%M")(set_color normal)
end

function fish_prompt
	echo (echo -e "\e$PROMPT_COLOR")(whoami)@(hostname|cut -d . -f 1) \
	     (set_color normal)(prompt_pwd)(echo -e "\e$PROMPT_COLOR")\$ (set_color normal)
end

function -v _ screen_title
	if test $TERM = screen
		echo -ne "\ek$_\e\\"
	end
end

