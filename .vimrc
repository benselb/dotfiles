set nocp

set rtp+=~/.vim/bundle/Vundle.vim
filetype off " required for Vundle
call vundle#begin()

" Vundle itself
Plugin 'VundleVim/Vundle.vim'

" Snippets
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'

Plugin 'fatih/vim-go'

source ~/.vim-formatting
if filereadable($HOME . "/.vimrc-local")
	source ~/.vimrc-local
endif

call vundle#end()
filetype plugin indent on

set nowrap
set ai ci pi
set showtabline=2
set bs=eol,indent,start

set wildmode=longest,list
set wildmenu

set ruler

colorscheme desert

if has("multi_byte") && &encoding == "utf-8"
	set list listchars=tab:·\ ,trail:␣
else
	set list listchars=tab:.\ ,trail:_
endif

inoremap <C-Space> <C-x><C-o>
imap <C-@> <C-Space>

if has("gui_running")
	set guioptions-=T
elseif &term == "screen"
	set t_Co=16
	set t_ts=k
	set t_fs=\
	set title
	set titlestring=%t
elseif &term =~ "-256color$"
  set t_Co=256
  colorscheme monokai
elseif &term == "xterm"
	set t_Co=16
endif

if has("mouse")
	set mouse=a
endif

syntax on

nmap <Leader>o :set list! paste!<CR>
map <Leader>x :!xclip -i<CR>u
nmap <Leader>[ :cN<CR>
nmap <Leader>] :cn<CR>
nmap <C-p> :FZF<CR>

if exists("+colorcolumn")
	set colorcolumn=81
	highlight colorcolumn ctermbg=8
endif

if has("extra_search")
  set incsearch
  set hlsearch
endif

if executable("ack")
  set grepprg=ack\ -k
endif

let g:go_fmt_command = "goimports"

autocmd VimResized * wincmd =

autocmd BufNewFile *.pl,*.pm call append(0, 'use warnings;')
autocmd BufNewFile *.pl,*.pm call append(1, 'use strict;')
autocmd BufNewFile *.pl call append(0, '#!/usr/bin/env perl')
autocmd BufNewFile *.pm call append(3, 'package ')
autocmd BufNewFile *.pm call append(4, '1;')
