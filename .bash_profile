# lightdm likes to set the locale this way. This works with almost everything,
# but occasionally an application will misbehave because it's not in the
# canonical format.
if [ "$LANG" = "en_US.utf8" ]; then
  LANG=en_US.UTF-8
fi

[ -f "$HOME/.bashrc" ] && . "$HOME/.bashrc"
